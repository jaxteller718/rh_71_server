﻿Key,Source,Context,English
q_SurvivalJournalRH,Quest,Quest Info,Survival Journal,,,,,

q_SurvivalJournal1RH,Quest,Quest Info,Creating a Journal,,,,,
q_SurvivalJournal1RH_subtitle,Quest,Quest Info,Creating a Journal,,,,,
q_SurvivalJournal1RH_description,Quest,Quest Info,"I guess I'm starting from scratch. First, I'll need to find some paper and a way to bind it.\n\n[eaa1a1]Note: After completing making the journal go into your bag and read it to continue this questline.",,,,,
q_SurvivalJournal1RH_offer,Quest,Quest Info,"I guess I'm starting from scratch. First, I'll need to find some paper and a way to bind it.",,,,,

q_SurvivalJournal2RH,Quest,Quest Info,Gathering Information,,,,

q_SurvivalJournal2RH_subtitle,Quest,Quest Info,Gathering Information,,,,
q_SurvivalJournal2RH_description,Quest,Quest Info,"I remember Old Navezgane. Before the Mists War started. Its a shell of its former self now but I should make a drawing to remember, just in case I need to go there again. After I am done I should collect the rest of the things I will need to get started on my journey.".,,,,
q_SurvivalJournal2RH_offer,Quest,Quest Info,"This is a good time to start taking notes on how to survive. After I make this map I will collect the things I need to start my journal and survive the rest of today.",,,,

q_SurvivalJournal3RH,Quest,Quest Info,Journal Chapter Entries,,,,,

q_SurvivalJournal3RH_subtitle,Quest,Quest Info,Journal Chapter Entries,,,,,
q_SurvivalJournal3RH_description,Quest,Quest Info,"Well, I could start with 'Dear diary'.\n\nEach chapter can cover a topic. I just need to find subjects for each chapter. Time to look around for inspiration.",,,,,
q_SurvivalJournal3RH_offer,Quest,Quest Info,"Well, I could start with 'Dear diary'.\n\nEach chapter can cover a topic. I just need to find subjects for each chapter. Time to look around for inspiration.",,,,,

q_SurvivalJournal4RH,Quest,Quest Info,Completing Your Journal,,,,,

q_SurvivalJournal4RH_subtitle,Quest,Quest Info,Completing Your Journal,,,,,
q_SurvivalJournal4RH_description,Quest,Quest Info,"Now that you have learned the basics of Ravenhearst, it is time to take the knowledge you have learned and complete your journal. When you have acquired all Completed Journal Chapters your training will be complete and you will receive a special gift left in your care by two brothers who devoted their lives to stopping this Apocalypse, but ultimately failed. \n\nThey have entrusted to you. Use it wisely.",,,,,
q_SurvivalJournal4RH_offer,Quest,Quest Info,"Now that you have learned the basics and some new tricks as well, it is time to complete your journey. When you have acquired all Completed Journal Entries combine them and you will unlock some nice rewards.",,,,,

q_journalAdvancedSurvivalRH,Quest,Quest Info,Advanced Survival Tactics,,,,,


q_journalAdvancedSurvival1RH,Quest,Quest Info,Craft a Survival Campfire,,,,,
q_journalAdvancedSurvival1RH_offer,Quest,Quest Info,"The survival campfire can cook more advanced recipes than the campfire and is very important to survival.",,,,,
q_journalAdvancedSurvival1RH_subtitle,Quest,Quest Info,Craft a Survival Campfire,,,,,
q_journalAdvancedSurvival1RH_description,Quest,Quest Info,"The survival campfire can cook more advanced recipes than the campfire and is very important to survival.",,,,,

q_journalAdvancedSurvival3RH,Quest,Quest Info,Scrap A Schematic and Collect KP,,,,,
q_journalAdvancedSurvival3RH_subtitle,Quest,Quest Info,Scrap A Schematic and Collect KP,,,,,
q_journalAdvancedSurvival3RH_description,Quest,Quest Info,"Scrapping Schematics can give you Knowledge Points that you use to craft quests and schematic volumes at a research desk.",,,,,
q_journalAdvancedSurvival3RH_offer,Quest,Quest Info,"Absorbing the knowledge through scrapping books is a great way to gain knowledge. The more knowledge you gain the easier it will be to learn exclusive recipes and more. Take an unneeded schematic and scrap it to gain Knowledge Points that can be used in the Research Desk.",,,,,

q_journalAdvancedSurvival4RH,Quest,Quest Info,"Filtering The Water",,,,,
q_journalAdvancedSurvival4RH_offer,Quest,Quest Info,"Ever since the outbreak began reports were that the infection initially spread through the waters around Ravenhearst. Makasin Pharmaceuticals were seen dumping chemicals into the rivers. In order to ensure your safety you will need to initiate a process to filter the outbreak from the waters in the town.\n\nConstructing a special Water Filtration Unit should be good enough to rid the water of the infection but it won't be enough to make it safe.\n\n[eaa1a1]Recipes can have more than 5 ingredients now. In order to tell which ones do look for the little arrow in the description. If an arrow is present it means you can switch pages to see the rest of the ingredients.",,,,,
q_journalAdvancedSurvival4RH_description,Quest,Quest Info,"Craft A Water Filtration Unit.",,,,,
q_journalAdvancedSurvival4RH_subtitle,Quest,Quest Info,Infected Waters,,,,,

q_journalAdvancedSurvival5RH,Quest,Quest Info,Cleaning The Water,,,,,
q_journalAdvancedSurvival5RH_offer,Quest,Quest Info,"Now that you have ensured the water is free from the outbreak, you will need to take that water and boil it so that any leftover bacteria in it will be thoroughly cooked out. Take the water you just cleansed and boil it over a fire. It will then be safe to drink.",,,,,
q_journalAdvancedSurvival5RH_description,Quest,Quest Info,"Boil Your Water.",,,,,
q_journalAdvancedSurvival5RH_subtitle,Quest,Quest Info,Infected Waters,,,,,

q_journalAdvancedSurvival6RH,Quest,Quest Info,The War For Gas,,,,,
q_journalAdvancedSurvival6RH_offer,Quest,Quest Info,"Gas is going to become very valuable as you obtain vehicles and powered tools. Unfortunately gas is in limited supply. You are going to have to fight for every drop. Take 50 empty gas cans that you have found or made and go to a gas pump. Once the pump is empty use the gas cans on it. The pump will begin to fill.\nOnce the pump is filled you will be able to loot it for some gas. You can repeat this process as often as you can.",,,,,
q_journalAdvancedSurvival6RH_description,Quest,Quest Info,"Gas is going to become very valuable as you obtain vehicles and powered tools. Unfortunately gas is in limited supply. You are going to have to fight for every drop. Take 50 empty gas cans that you have found or made and go to a gas pump. Once the pump is empty use the gas cans on it. The pump will begin to fill.\nOnce the pump is filled you will be able to loot it for some gas. You can repeat this process as often as you can.",,,,,
q_journalAdvancedSurvival6RH_subtitle,Quest,Quest Info,The War For Gas,,,,,

q_journalFarmingRH,Quest,Quest Info,Farming Journal,,,,,

q_journalFarming1RH,Quest,Quest Info,Making A Farm Table,,,,,
q_journalFarming1RH_offer,Quest,Quest Info,"The farm table is a farmers hub for producing seeds and resources. It is essential to build one.",,,,,
q_journalFarming1RH_description,Quest,Quest Info,"The farm table is a farmers hub for producing seeds and resources. It is essential to build one.",,,,,
q_journalFarming1RH_subtitle,Quest,Quest Info,Making A Farm Table,,,,,



q_journalFarming2RH,Quest,Quest Info,Starting your Farm,,,,,
q_journalFarming2RH_offer,Quest,Quest Info,"Before I start my farm I am going to want to make a Composter.\n\nA Composter uses compost to create various amounts of Fertilizer. The Composter must be placed in a well ventilated area with access to sunlight on soil.",,,,,
q_journalFarming2RH_description,Quest,Quest Info,"Before I start my farm I am going to want to make a Composter.\n\nA Composter uses compost to create various amounts of Fertilizer. The Composter must be placed in a well ventilated area with access to sunlight on soil.",,,,,
q_journalFarming2RH_subtitle,Quest,Quest Info,Starting your Farm,,,,,

q_journalFarming3RH,Quest,Quest Info,Start Composting,,,,,
q_journalFarming3RH_offer,Quest,Quest Info,"Now that I have a Composter I will need Compost to produce Fertilizer. It shouldn't take me long to gather what I need to make some compost.\n\nHolding the Compost in my hand use the Action Button to place it in the Composter.It will take some time to produce Fertilizer.",,,,,
q_journalFarming3RH_description,Quest,Quest Info,"Now that I have a Composter I will need Compost to produce Fertilizer. It shouldn't take me long to gather what I need to make some compost.\n\nHolding the Compost in my hand use the Action Button to place it in the Composter.It will take some time to produce Fertilizer.",,,,,
q_journalFarming3RH_subtitle,Quest,Quest Info,Start Composting,,,,,

q_journalFarming4RH,Quest,Quest Info,Till The Land,,,,,
q_journalFarming4RH_offer,Quest,Quest Info,"You can till land to start your farm. You will need to make a bucket of water and use it next to tilled soil in order to grow your seeds. If you do not use water your seed will never grow. Find a hoe and till some land.",,,,,
q_journalFarming4RH_description,Quest,Quest Info,"You can till land to start your farm. You will need to make a bucket of water and use it next to tilled soil in order to grow your seeds. If you do not use water your seed will never grow. Find a hoe and till some land.",,,,,
q_journalFarming4RH_subtitle,Quest,Quest Info,Till The Land,,,,,

q_journalFarming5RH,Quest,Quest Info,Making Seeds,,,,,
q_journalFarming5RH_offer,Quest,Quest Info,"Now it is time to use that farm plot. Take the seed pack you were given and use it in the farm table. Craft a Blueberry Seed and plant it in the farm plot. Then in a few hours it will grow and can be picked. Here are some extra seed packs as well to help you get your farm started.",,,,,
q_journalFarming5RH_description,Quest,Quest Info,"Now it is time to use that farm plot. Take the seed pack you were given and use it in the farm table. Craft a Blueberry Seed and plant it in the farm plot. Then in a few hours it will grow and can be picked. Here are some extra seed packs as well to help you get your farm started.",,,,,
q_journalFarming5RH_subtitle,Quest,Quest Info,Making Seeds,,,,,

q_journalFarming6RH,Quest,Quest Info,Preserving Crops,,,,,
q_journalFarming6RH_offer,Quest,Quest Info,"You are now on your way to sustaianble food. But as you know crops expire pretty quicky. Luckily you can craft a preservation barrel that will help your crops last a bit longer. This should get you by until you have enough crops collected to make multiples. Go ahead and craft and place one and start living off the land.",,,,,
q_journalFarming6RH_description,Quest,Quest Info,"You are now on your way to sustaianble food. But as you know crops expire pretty quicky. Luckily you can craft a preservation barrel that will help your crops last a bit longer. This should get you by until you have enough crops collected to make multiples. Go ahead and craft and place one and start living off the land.",,,,,
q_journalFarming6RH_subtitle,Quest,Quest Info,Preserving Crops,,,,,


q_journalStationsRH,Quest,Quest Info,Building Workstations,,,,,

q_journalStations1RH,Quest,Quest Info,Craft a Tanning Rack,,,,,
q_journalStations1RH_offer,Quest,Quest Info,"Fighting isn't the only thing I need to survive. I have to rebuild.\n\nIn order to rebuild, I'll need the tools and stations to do that.",,,,,
q_journalStations1RH_subtitle,Quest,Quest Info,Craft a Tanning Rack,,,,,
q_journalStations1RH_description,Quest,Quest Info,"A tanning rack is your primary source for curing hides and leather. Kill an animal and grab some hide and craft a tanning rack.",,,,,

q_journalStations2RH,Quest,Quest Info,Craft a Forge,,,,,
q_journalStations2RH_offer,Quest,Quest Info,"A forge is used to smelt basic metals and craft just the basics. You will need a blacksmith forge to craft more complex materials and later on a blast furnace.",,,,,
q_journalStations2RH_subtitle,Quest,Quest Info,Craft a Forge,,,,,
q_journalStations2RH_description,Quest,Quest Info,"A forge is used to smelt basic metals and craft just the basics. You will need a blacksmith forge to craft more complex materials and later on a blast furnace.",,,,,

q_journalStations2aRH,Quest,Quest Info,Create A Mold,,,,,
q_journalStations2aRH_subtitle,Quest,Quest Info,Create A Mold,,,,,
q_journalStations2aRH_description,Quest,Quest Info,"Molds are used to shape items so you can craft them.",,,,,
q_journalStations2aRH_offer,Quest,Quest Info,"Creating a blank mold will allow you to use it in the creation of additional molds. Craft a blank mold, which you can then use with an item to make a mold out of it. That mold can be placed in forges and is used to create additional items like arrowheads and jars.",,,,,


q_journalStations3RH,Quest,Quest Info,Craft a Table Saw,,,,,
q_journalStations3RH_subtitle,Quest,Quest Info,Craft a Table Saw,,,,,
q_journalStations3RH_description,Quest,Quest Info,"A table saw is mandatory for crafting wood planks which is used in many recipes. You can also use it to create new storage and decorative items.",,,,,
q_journalStations3RH_offer,Quest,Quest Info,"A table saw is mandatory for crafting wood planks which is used in many recipes. You can also use it to create new storage and decorative items.",,,,,

q_journalStations4RH,Quest,Quest Info,Craft a Workbench,,,,,
q_journalStations4RH_subtitle,Quest,Quest Info,Craft a Workbench,,,,,
q_journalStations4RH_description,Quest,Quest Info,"A workbench not only can craft items from you personal bench but it is used to craft more complex recipes.",,,,,
q_journalStations4RH_offer,Quest,Quest Info,"A workbench not only can craft items from you personal bench but it is used to craft more complex recipes.",,,,,

q_journalStations5RH,Quest,Quest Info,Craft an Armor Bench,,,,,
q_journalStations5RH_subtitle,Quest,Quest Info,Craft an Armor Bench,,,,,
q_journalStations5RH_description,Quest,Quest Info,"An Armor Bench is crucial to crafting better armor and clothing.",,,,,
q_journalStations5RH_offer,Quest,Quest Info,"An Armor Bench is crucial to crafting better armor and clothing.",,,,,

q_journalStations5aRH,Quest,Quest Info,Craft a Chemistry Station,,,,,
q_journalStations5aRH_subtitle,Quest,Quest Info,Craft a Chemistry Station,,,,,
q_journalStations5aRH_description,Quest,Quest Info,"The chemistry station is the epicenter of being able to stay alive. From gas production to healing items and antibiotics, no one should be without one.",,,,,
q_journalStations5aRH_offer,Quest,Quest Info,"The chemistry station is the epicenter of being able to stay alive. From gas production to healing items and antibiotics, no one should be without one.",,,,,

q_journalStations6RH,Quest,Quest Info,Craft a Blacksmith Forge,,,,,
q_journalStations6RH_subtitle,Quest,Quest Info,Craft a Blacksmith Forge,,,,,
q_journalStations6RH_description,Quest,Quest Info,"A Blacksmith Forge unlocks access to more forged recipes like Steel.",,,,,
q_journalStations6RH_offer,Quest,Quest Info,"A Blacksmith Forge unlocks access to more forged recipes like Steel.",,,,,

q_journalFoodRH,Quest,Quest Info,Cooking in the Wastelands,,,,,

q_journalFood1RH,Quest,Quest Info,Fry Some Insects,,,,,
q_journalFood1RH_description,Quest,Quest Info,"Often you will find yourself needing food in a pinch. Punching grass will sometimes yield you insects. These can be eaten as is for a small food boost or you can take your collection and a can or jar and fry them in a campfire. Frying them reduces their food benefit slightly but will reduce the chance of illness and will offer a small boost in health as well.",,,,,
q_journalFood1RH_subtitle,Quest,Quest Info,Fry Some Insects,,,,,
q_journalFood1RH_offer,Quest,Quest Info,"Often you will find yourself needing food in a pinch. Punching grass will sometimes yield you insects. These can be eaten as is for a small food boost or you can take your collection and a can or jar and fry them in a campfire. Frying them reduces their food benefit slightly but will guarantee no illness and will offer a small boost in health as well.",,,,,

q_journalFood2RH,Quest,Quest Info,Catch More Insects,,,,,
q_journalFood2RH_description,Quest,Quest Info,"Now that you have tasted those buggy little morsels I know you can not get enough of it. An alternate to punching grass is leaving out insect traps using honey or beer. Try it. Craft a Beer Trap and some refill for it and place it. You can use the refill on the trap to upgrade it after you have taken the insects you caught.",,,,,
q_journalFood2RH_subtitle,Quest,Quest Info,Catch More Insects,,,,,
q_journalFood2RH_offer,Quest,Quest Info,"Now that you have tasted those buggy little morsels I know you can not get enough of it. An alternate to punching grass is leaving out insect traps using honey or beer. Try it. Craft a Beer Trap and some refill for it and place it. You can use the refill on the trap to upgrade it after you have taken the insects you caught.",,,,,

q_journalFood3RH,Quest,Quest Info,Craft Some Survival Mix,,,,,
q_journalFood3RH_description,Quest,Quest Info,"Aside from making jerky in a rack, another way you can make nature work for you is by collecting 5 of any seed type from trees and combining them with blueberries to make survival mix. Make some survival mix now.",,,,,
q_journalFood3RH_subtitle,Quest,Quest Info,Craft Some Survival Mix,,,,,
q_journalFood3RH_offer,Quest,Quest Info,"Aside from making jerky in a rack, another way you can make nature work for you is by collecting 5 of any seed type from trees and combining them with blueberries to make survival mix. Make some survival mix now.",,,,,

q_journalFood4RH,Quest,Quest Info,Spear Some Fish,,,,,
q_journalFood4RH_description,Quest,Quest Info,"Fishing is extremely important to survival. You can dig up some worms by using a shovel on dirt. Craft a Fishing Spear and find a body of water. Left Click on the water with the spear until you see a fish icon. You will have a short period of time to right click the fish off of your spear, placing a bucket in your inventory. Place the bucket on the ground then use a knife on the bucket to see if you caught anything!",,,,,
q_journalFood4RH_subtitle,Quest,Quest Info,Spear Some Fish,,,,,
q_journalFood4RH_offer,Quest,Quest Info,"Fishing is extremely important to survival. You can dig up some worms by using a shovel on dirt. Craft a Fishing Spear and find a body of water. Left Click on the water with the spear until you see a fish icon. You will have a short period of time to right click the fish off of your spear, placing a bucket in your inventory. Place the bucket on the ground then use a knife on the bucket to see if you caught anything!",,,,,

q_journalFood5RH,Quest,Quest Info,Use A Fishing Rod,,,,,
q_journalFood5RH_description,Quest,Quest Info,"Fishing is extremely important to survival. You can dig up some worms by using a shovel on dirt. Craft a Fishing Rod and find a body of water. Right Click on the water with the pole until you see a fish icon. With the full rod in your hand right click the ground in front of you. It will place a bucket. Use a knife on the bucket to see if you caught anything!",,,,,
q_journalFood5RH_subtitle,Quest,Quest Info,Use A Fishing Rod,,,,,
q_journalFood5RH_offer,Quest,Quest Info,"Fishing is extremely important to survival. You can dig up some worms by using a shovel on dirt. Craft a Fishing Rod and find a body of water. Right Click on the water with the pole until you see a fish icon. With the full rod in your hand right click the ground in front of you. It will place a bucket. Use a knife on the bucket to see if you caught anything!",,,,,

q_journalAdditonalFood1RH,Quest,Quest Info,Additional Food: Rain Collector,,,,,
q_journalAdditonalFood1RH_description,Quest,Quest Info,"Water is vital to survival but you won't always have running water or a water source. The rain collector is a great way to catch water for use in the filtration unit. Using some tarps you can upgrade an empty rain collector. In a few hours it should yield various water for you to collect. The process can be repeated by using more tarps.",,,,,
q_journalAdditonalFood1RH_subtitle,Quest,Quest Info,Additional Food: Rain Collector,,,,,
q_journalAdditonalFood1RH_offer,Quest,Quest Info,"Water is vital to survival but you won't always have running water or a water source. The rain collector is a great way to catch water for use in the filtration unit. Using some tarps you can upgrade an empty rain collector. In a few hours it should yield various water for you to collect. The process can be repeated by using more tarps.",,,,,

q_journalAdditonalFood2RH,Quest,Quest Info,Additional Food: Bee Hives,,,,,
q_journalAdditonalFood2RH_description,Quest,Quest Info,"Honey is a leading source of both food recipes and infection cures. You can start a bee hive by collecting bees and using them to upgrade a beehive. After some times has passed you will be able to collect honeycomb from it. This process can be repeated with more bees.",,,,,
q_journalAdditonalFood2RH_subtitle,Quest,Quest Info,Additional Food: Bee Hives,,,,,
q_journalAdditonalFood2RH_offer,Quest,Quest Info,"Honey is a leading source of both food recipes and infection cures. You can start a bee hive by collecting bees and using them to upgrade a beehive. After some times has passed you will be able to collect honeycomb from it. This process can be repeated with more bees.",,,,,

q_journalAdditonalFood3RH,Quest,Quest Info,Additional Food: Microwaves,,,,,
q_journalAdditonalFood3RH_description,Quest,Quest Info,"There are lots of cans you can collect in the world. Finding a working microwave in the world is the best use of these cans. You can take a few cans and turn them into a nutritious filling meal. Find a microwave and bring it back home by destroying microwaves you find in houses. Be careful though, you may not always receive a working one on your first try.",,,,,
q_journalAdditonalFood3RH_subtitle,Quest,Quest Info,Additional Food: Microwaves,,,,,
q_journalAdditonalFood3RH_offer,Quest,Quest Info,"There are lots of cans you can collect in the world. Finding a working microwave in the world is the best use of these cans. You can take a few cans and turn them into a nutritious filling meal. Find a microwave and bring it back home by destroying microwaves you find in houses. Be careful though, you may not always receive a working one on your first try.",,,,,

q_journalAdditonalFood4RH,Quest,Quest Info,Additional Food: Food Sealer,,,,,
q_journalAdditonalFood4RH_description,Quest,Quest Info,"As you know by now food in the world spoils. You can preserve some meat and veggies by finding a working sealer in the world. To take one home destroy it. You may not always get a working one on your first try. Loot cling wrap to use with meat and veggies in your new sealer and food will no longer spoil until you are ready to use it.",,,,,
q_journalAdditonalFood4RH_subtitle,Quest,Quest Info,Additional Food: Food Sealer,,,,,
q_journalAdditonalFood4RH_offer,Quest,Quest Info,"As you know by now food in the world spoils. You can preserve some meat and veggies by finding a working sealer in the world. To take one home destroy it. You may not always get a working one on your first try. Loot cling wrap to use with meat and veggies in your new sealer and food will no longer spoil until you are ready to use it.",,,,,

q_journalCoverRH,Items,Item,Empty Survival Journal,,,,,
q_journalCoverRHDesc,Items,Item,A binder to hold all of your notes on survival in the apocalypse. You'll need something to write with to start the journal. When you finish a questline you will receive a completed chapter. Hold on to these! You will need them to craft the completed journal later on.,,,,,
questItemInkPenRH,Items,Item,Ink Pen,,,,,
questItemInkPenRHDesc,Items,Item,This ink pen is nearly dry. You'll have to be careful how you use it.,,,,,
questItemJournalRH,Items,Item,Survival Journal,,,,,
questItemJournalRHDesc,Items,Item,"The journal is now ready to be written in. As you craft your chapters you'll receive quest lines from them. When you finish a quest line you will receive completed chapters. Hold on to these along with this book to craft your completed journal later on.",,,,,
journalFoodRH,Items,Item,Food Chapter,,,,,
journalFoodRHDesc,Items,Item,"With all of the restaurants closed, it's time to start improvising new things to eat. This bug has to be... nutritious, right?",,,,,
journalStationsRH,Items,Item,Workstation Chapter,,,,,
journalStationsRHDesc,Items,Item,"A simple worktable will do to start, but there has to be other options.",,,,,
journalFarmingRH,Items,Item,Farming Chapter,,,,,
journalFarmingRHDesc,Items,Item,"Farming will be a task in itself. Let's see if we can find some seeds to start us off." ,,,,,
journalAdvancedSurvivalRH,Items,Item,Advanced Survival Chapter,,,,,
journalAdvancedSurvivalRHDesc,Items,Item,"There are quite a few new ways to survive in this world. I should write them all down." ,,,,,
resourceCompletedChaptersRH,Items,Item,Completed Journal Chapter,,,,,
resourceCompletedChaptersRHDesc,Items,Item,This is a completed journal chapter. Hold on to this. It will be needed to complete the final step of the journal questline.,,,,,
completeJournalRH,Items,Item,Completed Journal,,,,,
completeJournalRHDesc,Items,Item,You have travelled a long road prospect. Your training is now complete. Enjoy these gifts I leave to you. You can take this book and scrap it for a nice boost in your knowledge.,,,,,

resourceChaptersRH,Items,Item,Journal Chapters,,,,,
resourceChaptersRHDesc,Items,Item,"You've split the journal into chapters that correspond to different specialties. You may be able to make more if you can find a research table.",,,,,


