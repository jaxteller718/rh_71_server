﻿Key,Source,Context,English
meleeToolRepairT0ScrapAxeRH,items,Tool,Scrap Axe,,,,,
meleeToolRepairT0ScrapAxeRHDesc,items,Tool,"The best way to gather wood from trees. Gives you a better chance at oak wood. \Repair with Iron.",,,,,
meleeToolPickT0ScrapPickaxeRH,items,Tool,Scrap Pickaxe,,,,,
meleeToolPickT0ScrapPickaxeRHDesc,items,Tool,"The best way to gather stone and ore from rocks and boulders early on. \Repair with Iron.",,,,,
meleeToolShovelT0ScrapShovelRH,items,Tool,Scrap Shovel,,,,,
meleeToolShovelT0ScrapShovelRHDesc,items,Tool,"The best way to dig out ditches early on. \Repair with Iron.",,,,,
